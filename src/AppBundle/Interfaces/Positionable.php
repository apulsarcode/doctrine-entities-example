<?php

namespace AppBundle\Interfaces;

interface Positionable
{
    public function setPosition($position);

    public function getPosition();
}