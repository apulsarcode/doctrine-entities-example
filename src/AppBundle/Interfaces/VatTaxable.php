<?php

namespace AppBundle\Interfaces;

interface VatTaxable
{
    public function setVatTax($vatTax);

    public function getVatTax();
}