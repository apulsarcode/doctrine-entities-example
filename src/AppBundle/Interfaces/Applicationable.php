<?php

namespace AppBundle\Interfaces;

interface Applicationable
{
    public function setApplication($application);

    public function getApplication();
}