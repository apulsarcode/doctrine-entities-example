<?php

namespace AppBundle\Interfaces;

interface Timestampable
{
    public function setCreatedAt(\DateTime $createdAt);

    public function getCreatedAt();

    public function setUpdatedAt(\DateTime $updatedAt);

    public function getUpdatedAt();

    public function setDeletedAt(\DateTime $deletedAt);

    public function getDeletedAt();
}