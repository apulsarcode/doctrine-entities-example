#!/bin/env bash

bin/console generate:doctrine:crud --entity=AppBundle:Category --route-prefix=/category --with-write --overwrite --no-interaction
bin/console generate:doctrine:crud --entity=AppBundle:City --route-prefix=/city --with-write --overwrite --no-interaction
bin/console generate:doctrine:crud --entity=AppBundle:Country --route-prefix=/country --with-write --overwrite --no-interaction
bin/console generate:doctrine:crud --entity=AppBundle:Landing --route-prefix=/landing --with-write --overwrite --no-interaction
